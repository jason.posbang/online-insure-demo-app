<?php
namespace App\Models;

use CodeIgniter\Model;

class PayrollModel extends Model
{
    protected $table      = 'payrolls';
    protected $primaryKey = 'id';

    protected function initialize()
    {
        parent::initialize();

        $this->allowedFields = [
            'user_id',
            'commision_rate',
            'tax_rate',
            'sales',
            'commision',
            'total',
            'bonus',
            'start',
            'end'
        ];
    }
}
