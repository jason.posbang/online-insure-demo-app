<?php
namespace App\Models;

use CodeIgniter\Model;

class PayrollCustomerModel extends Model
{
    protected $table      = 'payroll_customers';
    protected $primaryKey = 'id';

    protected function initialize()
    {
        parent::initialize();

        $this->allowedFields = [
            'payroll_id',
            'customer_id'
        ];
    }
}
