<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Forge;
use CodeIgniter\Database\Migration;

class AddPayrollCustomerTable extends Migration
{
    /**
     * @var string[]
     */
    private array $tables;

    public function __construct(?Forge $forge = null)
    {
        parent::__construct($forge);

        /** @var \Config\Auth $authConfig */
        $authConfig   = config('Auth');
        $this->tables = $authConfig->tables;
    }

    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'payroll_id' => [
                'type'           => 'INT',
                'unsigned'       => true,
            ],
            'customer_id' => [
                'type'           => 'INT',
                'constraint'     => 3,
            ],

        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('payroll_customers');
    }

    public function down()
    {
        $this->forge->dropTable('payroll_customers');
    }
}
