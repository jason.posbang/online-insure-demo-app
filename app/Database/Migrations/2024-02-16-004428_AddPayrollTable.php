<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Forge;
use CodeIgniter\Database\Migration;
use CodeIgniter\Database\RawSql;

class AddPayrollTable extends Migration
{
    /**
     * @var string[]
     */
    private array $tables;

    public function __construct(?Forge $forge = null)
    {
        parent::__construct($forge);

        /** @var \Config\Auth $authConfig */
        $authConfig   = config('Auth');
        $this->tables = $authConfig->tables;
    }

    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'user_id' => [
                'type'           => 'INT',
                'unsigned'       => true,
            ],
            'commision_rate' => [
                'type'           => 'INT',
                'constraint'     => 3,
            ],
            'tax_rate' => [
                'type'           => 'INT',
                'constraint'     => 3,
            ],
            'sales' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,2',
            ],
            'bonus' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,2',
            ],
            'commision' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,2',
            ],
            'total' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,2',
            ],
            'start' => [
                'type'           => 'DATETIME',
            ],
            'end' => [
                'type'           => 'DATETIME',
            ],
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'default'        => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
            ],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('payrolls');
    }

    public function down()
    {
        $this->forge->dropTable('payrolls');
    }
}
