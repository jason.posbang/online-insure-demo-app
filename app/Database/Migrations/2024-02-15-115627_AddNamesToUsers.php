<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Forge;
use CodeIgniter\Database\Migration;

class AddNamesToUsers extends Migration
{
    /**
     * @var string[]
     */
    private array $tables;

    public function __construct(?Forge $forge = null)
    {
        parent::__construct($forge);

        /** @var \Config\Auth $authConfig */
        $authConfig   = config('Auth');
        $this->tables = $authConfig->tables;
    }

    public function up()
    {
        $fields = [
            'firstname' => ['type' => 'VARCHAR', 'constraint' => '90', 'null' => true],
            'lastname' => ['type' => 'VARCHAR', 'constraint' => '50', 'null' => true],
            'contact_number' => ['type' => 'VARCHAR', 'constraint' => '20', 'null' => true],
        ];
        $this->forge->addColumn($this->tables['users'], $fields);
    }

    public function down()
    {
        $fields = [
            'firstname', 'lastname', 'contact_number'
        ];
        $this->forge->dropColumn($this->tables['users'], $fields);
    }
}
