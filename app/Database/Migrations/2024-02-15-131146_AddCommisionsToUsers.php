<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Forge;
use CodeIgniter\Database\Migration;

class AddCommisionsToUsers extends Migration
{
    /**
     * @var string[]
     */
    private array $tables;

    public function __construct(?Forge $forge = null)
    {
        parent::__construct($forge);

        /** @var \Config\Auth $authConfig */
        $authConfig   = config('Auth');
        $this->tables = $authConfig->tables;
    }

    public function up()
    {
        $fields = [
            'commision_rate' => ['type' => 'INT', 'constraint' => 3, 'null' => true],
            'tax_rate' => ['type' => 'INT', 'constraint' => 3, 'null' => true],
        ];
        $this->forge->addColumn($this->tables['users'], $fields);
    }

    public function down()
    {
        $fields = [
            'commision_rate', 'tax_rate'
        ];
        $this->forge->dropColumn($this->tables['users'], $fields);
    }
}
