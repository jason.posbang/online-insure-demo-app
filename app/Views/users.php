<?php include('common/header.php'); ?>

<body >

    <div id="app">
        <el-container>
            <el-header>
                <section style="display: flex; gap: 16px; align-items: center;">
                    <el-image src="assets/images/logo-image.png">
                    </el-image>
                    <h2>Online Insure</h2>
                </section>
                
                <section>
                    <el-menu
                    :default-active="activeIndex"
                    class="el-menu-demo"
                    mode="horizontal"
                    :ellipsis="false"
                    @select="handleSelect">
                        <el-menu-item index="1">Home</el-menu-item>
                        <el-menu-item index="2">Sales Rep</el-menu-item>
                        <el-menu-item index="3">Payroll</el-menu-item>
                        <el-menu-item index="4">PDF</el-menu-item>
                        <el-menu-item index="5">Logout</el-menu-item>
                    </el-menu>

                </section>
            </el-header>

            <el-main v-loading="loadingForm">
                <section style="display: flex; flex-direction: row-reverse; margin-bottom: 1.2rem; width: 100;">
                    <el-button :disabled="loadingForm" type="primary" @click="onAdd" size="large">Add</el-button>
                </section>

                <el-table 
                max-height="80vh"
                border
                :data="users">
                    <el-table-column prop="firstname" label="Firstname" min-width="140" sortable>
                    </el-table-column>

                    <el-table-column prop="lastname" label="Lastname" min-width="140" sortable>
                    </el-table-column>

                    <el-table-column prop="commision_rate" label="Commision Rate (%)" min-width="100" sortable>
                    </el-table-column>

                    <el-table-column prop="tax_rate" label="Tax Rate (%)" min-width="100" sortable>
                    </el-table-column>

                    <el-table-column label="" :min-width="70">
                        <template #default="scope">

                            <el-button @click="onEdit(scope.row)" 
                            :disabled="loadingForm">
                                Edit
                            </el-button>

                            <el-popconfirm 
                            @confirm="onDelete(scope.row)"
                            title="Are you sure to delete this?">
                                <template #reference>
                                    <el-button 
                                    type="warning" 
                                    :disabled="loadingForm">
                                        Delete
                                    </el-button>
                                </template>
                            </el-popconfirm>

                        </template>
                    </el-table-column>
                </el-table>
            </el-main>
        </el-container>
        
        <el-dialog v-model="showForm" 
        :title="ruleForm.editing ? 'Edit' : 'Add'" 
        width="500px">
            <el-form 
            v-loading="loadingForm"
            :model="ruleForm" 
            :rules="formRules" 
            label-width="150px"
            ref="ruleFormRef">

                <el-form-item v-if="!ruleForm.editing" label="Username" prop="username">
                    <el-input v-model="ruleForm.username" :disabled="ruleForm.editing" clearable>
                    </el-input>
                </el-form-item>
                
                <el-form-item label="Firstname" prop="firstname">
                    <el-input v-model="ruleForm.firstname" clearable>
                    </el-input>
                </el-form-item>

                <el-form-item label="Lastname" prop="lastname" clearable>
                    <el-input v-model="ruleForm.lastname">
                    </el-input>
                </el-form-item>

                <el-form-item label="Commision Rate (%)" prop="commision_rate">
                    <el-input-number 
                    v-model="ruleForm.commision_rate"
                    :min="0"
                    :max="100">
                    </el-input-number>
                </el-form-item>

                <el-form-item label="Tax Rate (%)" prop="tax_rate">
                    <el-input-number 
                    v-model="ruleForm.tax_rate"
                    :min="0"
                    :max="100">
                    </el-input-number>
                </el-form-item>
                
            </el-form>

            <section style="display: flex; flex-direction: row-reverse; gap: 16px; margin-bottom: 1.2rem; width: 100;">
                <el-button @click="showForm = !showForm" :disabled="loadingForm" type="warning">Cancel</el-button>
                <el-button @click="onSubmit" type="primary" :disabled="loadingForm">Submit</el-button>
            </section>
        </el-dialog>
    </div>

    <script>
        const { createApp, ref, reactive } = Vue
        const ElNotification = ElementPlus.ElNotification
        const ElLoading = ElementPlus.ElLoading
        var phpData = <?php echo json_encode(['users' => $users, 'csrfToken' => csrf_hash()]); ?>

        const app = createApp({
            setup() {
                let defaultFormValue = {
                    username: null,
                    firstname: null,
                    lastname: null,
                    commision_rate: null,
                    tax_rate: null
                }
                
                const ruleFormRef = ref(null)
                const activeIndex = ref('2')
                const users = ref(phpData.users)
                const showForm = ref(false)
                const ruleForm = ref(defaultFormValue)
                const formRules = ref({
                    username: [
                        { required: true, message: 'Required', trigger: 'blur' },
                        { min: 1, max: 20, message: 'Length should be 4 to 20', trigger: 'change' },
                    ],
                    firstname: [
                        { required: true, message: 'Required', trigger: 'blur' },
                        { min: 1, max: 70, message: 'Length should atleast be 1', trigger: 'change' },
                    ],
                    lastname: [
                        { required: true, message: 'Required', trigger: 'blur' },
                        { min: 1, max: 50, message: 'Length should atleast be 1', trigger: 'change' },
                    ],
                })
                const loadingForm = ref(false)

                function handleSelect(value) {
                    if(value == '1') {
                        window.location.href = 'index.php';
                    }
                    else if(value == '2') {
                        window.location.href = 'users';
                    }
                    else if(value == '3') {
                        window.location.href = 'payroll';
                    }
                    else if(value == '5') {
                        window.location.href = 'logout';
                    }
                }

                function onAdd() {
                    ruleForm.value = {
                        username: null,
                        firstname: null,
                        lastname: null,
                        commision_rate: null,
                        tax_rate: null
                    }
                    ruleForm.value.editing = false
                    showForm.value = true
                }

                function onEdit(item) {
                    ruleForm.value = {
                        id: item.id,
                        username: item.username,
                        firstname: item.firstname,
                        lastname: item.lastname,
                        commision_rate: item.commision_rate,
                        tax_rate: item.tax_rate
                    }
                    
                    ruleForm.value.editing = true
                    showForm.value = true

                    ruleFormRef.value.clearValidate(['firstname', 'lastname'])
                }

                async function onSubmit() {
                    let valid = await ruleFormRef.value.validate()

                    if(valid) {
                        sendAxiosRequest()
                    } else {
                        ElNotification({
                            title: 'Error',
                            message: 'Please fill all fields',
                            type: 'error',
                        })
                    }
                }

                async function sendAxiosRequest() {
                    loadingForm.value = true
                    const loadingInstance = ElLoading.service({ fullscreen: true })
                    axios.defaults.headers.common['X-CSRF-TOKEN'] = phpData.csrfToken

                    var formdata = new FormData()
                    formdata.append('username', ruleForm.value.username)
                    formdata.append('firstname', ruleForm.value.firstname)
                    formdata.append('lastname', ruleForm.value.lastname)
                    formdata.append('commision_rate', ruleForm.value.commision_rate)
                    formdata.append('tax_rate', ruleForm.value.tax_rate)

                    if(ruleForm.value.editing) {
                        formdata.append('id', ruleForm.value.id)
                    }

                    axios.post(
                        '/users', 
                        formdata,//JSON.stringify(ruleForm.value),
                        { 
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        }
                    ).then(response => {
                        if(response.data.success && response.data.data) {
                            let result = response.data.data

                            if(ruleForm.value.editing) {
                                users.value.find(li => {
                                    if(li.id == result.id) {
                                        li = Object.assign(li, result)
                                        return true
                                    }
                                })
                            } else {
                                users.value.push(response.data.data)
                            }
                        }

                        showForm.value = false
                    })
                    .catch(error => {
                        console.error(error);
                        ElNotification({
                            title: 'Error',
                            message: 'Something went wrong.',
                            type: 'error',
                        })
                    }).finally(() => {
                        loadingForm.value = false
                        loadingInstance.close()
                    })
                }

                function onDelete(item) {
                    console.log("onDelete")
                    const loadingInstance = ElLoading.service({ fullscreen: true })
                    loadingForm.value = true

                    axios.delete(
                        '/users/' + item.id
                    ).then(response => {
                        console.log("onDelete res", response.data)

                        if(response.data.success) {
                            let index = users.value.findIndex(li => li.id == item.id)

                            console.log("onDelete res 2", index)

                            if(index >= 0) {
                                users.value.splice(index, 1)
                            }
                        }
                        showForm.value = false
                    })
                    .catch(error => {
                        console.error(error);
                        ElNotification({
                            title: 'Error',
                            message: 'Something went wrong.',
                            type: 'error',
                        })
                    }).finally(() => {
                        loadingForm.value = false
                        loadingInstance.close()
                    })
                }
                
                return {
                    ruleFormRef,
                    activeIndex,
                    users,
                    onAdd,
                    onEdit,
                    onDelete,
                    showForm,
                    ruleForm,
                    formRules,
                    onSubmit,
                    sendAxiosRequest,
                    handleSelect
                }
            }
        })

        app.use(ElementPlus);
        app.mount('#app')
    </script>
</body>


<?php include('common/footer.php'); ?>