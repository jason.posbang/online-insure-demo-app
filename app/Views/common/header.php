<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Online Insure</title>
    <meta name="description" content="A simple demo.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favico.ico">

    <!-- Vue3 CDN -->
    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    <!-- Element Ui CDN -->
    <link rel="stylesheet" href="https://unpkg.com/element-plus/dist/index.css">
    <script src="https://unpkg.com/element-plus"></script>
    <script src="https://unpkg.com/@element-plus/icons-vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    
    <!-- STYLES -->

    <style {csp-style-nonce}>
        html {
            font-family: Arial, Helvetica, sans-serif;
        }

        body {
            margin: 0;
        }

        .el-menu--horizontal.el-menu {
            border-bottom: none;
        }

        .el-container .el-header {
            display: flex;
            align-items: center;
            border-bottom: 1px solid var(--el-border-color);
            box-shadow: var(--el-box-shadow-light);
            justify-content: space-between;
            min-height: 70px;
            padding: 0 24px;
        }

        .el-container .el-header .el-image {
            height: 50px; 
            width: 60px;
        }
    </style>
</head>