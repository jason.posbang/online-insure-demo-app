<?php include('common/header.php'); ?>

<body >

    <div id="app">
        <el-container>
            <el-header>
                <section style="display: flex; gap: 16px; align-items: center;">
                    <el-image src="assets/images/logo-image.png">
                    </el-image>
                    <h2>Online Insure</h2>
                </section>

                <section>
                    <el-menu
                    :default-active="activeIndex"
                    class="el-menu-demo"
                    mode="horizontal"
                    :ellipsis="false"
                    @select="handleSelect">
                        <el-menu-item index="1">Home</el-menu-item>
                        <el-menu-item index="2">Sales Rep</el-menu-item>
                        <el-menu-item index="3">Payroll</el-menu-item>
                        <el-menu-item index="4">PDF</el-menu-item>
                        <el-menu-item index="5">Logout</el-menu-item>
                    </el-menu>

                </section>
            </el-header>

            <el-main>
                <el-tag type="primary" effect="dark" size="large" round>Highest Sales</el-tag>
                <el-row style="margin-top: 24px;" :gutter="12">
                    <el-col
                    v-for="(item, index) in sales"
                    :key="item.id"
                    :span="8">
                        <el-card :body-style="{ padding: '24px' }">
                            <div style="display: flex; gap: 8px; align-items: center;">
                                <el-avatar 
                                :size="50" 
                                src="https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png">
                                </el-avatar>

                                <div>
                                    <h3 style="margin: 0">{{ item.firstname }} {{ item.lastname }}</h3>
                                    <h4 style="margin: 5px 0 0">Sales: {{ item.sales }}</h3>
                                </div>
                            </div>

                        </el-card>
                    </el-col>
                </el-row>

                <el-tag type="primary" size="large" effect="dark" round style="margin-top: 56px">Highest Commission</el-tag>
                <el-row style="margin-top: 24px;" :gutter="12">
                    <el-col
                    v-for="(item, index) in commission"
                    :key="item.id"
                    :span="8">
                        <el-card :body-style="{ padding: '24px' }">
                            <div style="display: flex; gap: 8px; align-items: center;">
                                <el-avatar 
                                :size="50" 
                                src="https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png">
                                </el-avatar>

                                <div>
                                    <h3 style="margin: 0">{{ item.firstname }} {{ item.lastname }}</h3>
                                    <h4 style="margin: 5px 0 0;">Commission: {{ item.commision }}</h3>
                                </div>
                            </div>

                        </el-card>
                    </el-col>
                </el-row>
            </el-main>
        </el-container>
    </div>

    <script>
        const { createApp, ref } = Vue
        var phpData = <?php echo json_encode(['sales' => $sales, 'commission' => $commission, 'csrfToken' => csrf_hash()]); ?>

        const app = createApp({
            setup() {
                const activeIndex = ref('1')
                const sales = ref(phpData.sales)
                const commission = ref(phpData.commission)

                function handleSelect(value) {
                    console.log("handleSelect", value, value == '2')

                    if(value == '2') {
                        window.location.href = 'users';
                    }
                    else if(value == '3') {
                        window.location.href = 'payroll';
                    }
                    else if(value == '5') {
                        window.location.href = 'logout';
                    }
                }
                
                return {
                    activeIndex,
                    handleSelect,
                    sales,
                    commission
                }
            }
        })

        app.use(ElementPlus);
        
        app.mount('#app')
    </script>
</body>


<?php include('common/footer.php'); ?>