<?php include('common/header.php'); ?>

<body >

    <div id="app">
        <el-container>
            <el-header>
                <section style="display: flex; gap: 16px; align-items: center;">
                    <el-image src="assets/images/logo-image.png">
                    </el-image>
                    <h2>Online Insure</h2>
                </section>
                

                <section>
                    <el-menu
                    :default-active="activeIndex"
                    class="el-menu-demo"
                    mode="horizontal"
                    :ellipsis="false"
                    @select="handleSelect">
                        <el-menu-item index="1">Home</el-menu-item>
                        <el-menu-item index="2">Sales Rep</el-menu-item>
                        <el-menu-item index="3">Payroll</el-menu-item>
                        <el-menu-item index="4">PDF</el-menu-item>
                        <el-menu-item index="5">Logout</el-menu-item>
                    </el-menu>

                </section>
            </el-header>

            <el-main>
                <section style="display: flex; flex-direction: row-reverse; margin-bottom: 1.2rem; width: 100;">
                    <el-button :disabled="loadingForm" type="primary" @click="onAdd" size="large">Add</el-button>
                </section>

                <el-table 
                max-height="80vh"
                border
                :data="payrolls">
                    <el-table-column prop="firstname" label="Sales Rep" min-width="140" sortable>
                        <template #default="scope">
                            <span>{{ scope.row.firstname }} {{ scope.row.lastname }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column prop="customers" label="Customers" min-width="140">
                        <template #default="scope">
                            <p v-for="(customer) in scope.row.customers" style="margin: 0">
                                {{ customer.firstname }} {{ customer.lastname }}
                            </p>
                        </template>
                    </el-table-column>

                    <el-table-column prop="commision_rate" label="Commision Rate (%)" min-width="100">
                    </el-table-column>

                    <el-table-column prop="tax_rate" label="Tax Rate (%)" min-width="100">
                    </el-table-column>

                    <el-table-column prop="start" label="Period (Week)" min-width="100">
                        <template #default="scope">
                            <el-date-picker
                            v-model="scope.row.start"
                            style="width: 100%;"
                            type="week"
                            format="[Week] ww"
                            placeholder="Pick a week"
                            disabled>
                            </el-date-picker>
                        </template>
                    </el-table-column>

                    <el-table-column prop="sales" label="Sales this Period ($)" min-width="100">
                    </el-table-column>

                    <el-table-column prop="bonus" label="Bonus ($)" min-width="100">
                    </el-table-column>

                    <el-table-column prop="sales" label="Total Commission ($)" min-width="100">
                    </el-table-column>
                </el-table>
            </el-main>
        </el-container>

        <el-dialog 
        v-model="showForm" 
        title="Add Payroll" 
        width="520px">
            <el-form 
            v-loading="loadingForm"
            :model="ruleForm" 
            :rules="formRules" 
            label-width="160px"
            ref="ruleFormRef">

                <el-form-item label="Sales Rep" prop="user_id">
                    <el-select
                    v-model="ruleForm.user_id"
                    @change="onUserSelected"
                    placeholder="Please select">
                        <el-option
                        v-for="item in users"
                        :key="item.id"
                        :label="item.firstname + ' ' + item.lastname"
                        :value="item.id">
                        </el-option>
                    </el-select>
                </el-form-item>

                <el-form-item label="Customers" prop="customers">
                    <el-select
                    v-model="ruleForm.customers"
                    @change="onCustomerSelected"
                    multiple
                    placeholder="Please select">
                        <el-option
                        label="-- Add new --"
                        :value="0">
                        </el-option>

                        <el-option
                        v-for="item in customers"
                        :key="item.id"
                        :label="item.firstname + ' ' + item.lastname"
                        :value="item.id">
                        </el-option>
                    </el-select>
                </el-form-item>

                <el-form-item label="Commision Rate (%)" prop="commision_rate">
                    <el-input-number 
                    v-model="ruleForm.commision_rate"
                    @change="updateTotals"
                    :min="0"
                    :max="100">
                    </el-input-number>
                </el-form-item>

                <el-form-item label="Tax Rate (%)" prop="tax_rate">
                    <el-input-number 
                    v-model="ruleForm.tax_rate"
                    @change="updateTotals"
                    :min="0"
                    :max="100">
                    </el-input-number>
                </el-form-item>

                <el-form-item label="Period (Week)" prop="start">
                    <el-date-picker
                    v-model="ruleForm.start"
                    type="week"
                    format="[Week] ww"
                    placeholder="Pick a week">
                    </el-date-picker>
                </el-form-item>

                <el-form-item label="Sales this Period ($)" prop="sales">
                    <el-input-number 
                    v-model="ruleForm.sales"
                    @change="updateTotals"
                    controls-position="right"
                    :min="0"
                    :max="999999999">
                    </el-input-number>
                </el-form-item>

                <el-form-item label="Bonus ($)" prop="bonus">
                    <el-input-number 
                    v-model="ruleForm.bonus"
                    @change="updateTotals"
                    controls-position="right"
                    :min="0"
                    :max="9999999">
                    </el-input-number>
                </el-form-item>

                <el-form-item label="Total Commission ($)" prop="total">
                    <el-input-number 
                    v-model="ruleForm.total"
                    @change="updateTotals"
                    controls-position="right"
                    disabled
                    :min="0"
                    :max="9999999">
                    </el-input-number>
                </el-form-item>
                
            </el-form>

            <section style="display: flex; flex-direction: row-reverse; gap: 16px; margin-bottom: 1.2rem; width: 100;">
                <el-button @click="showForm = !showForm" :disabled="loadingForm" type="warning">Cancel</el-button>
                <el-button @click="onSubmit" type="primary" :disabled="loadingForm">Submit</el-button>
            </section>
        </el-dialog>

        <el-dialog 
        v-model="showCustomerForm" 
        append-to-body
        title="Add Customer" 
        width="300px"
        top="20vh">
            <el-form 
            v-loading="loadingForm"
            :model="customerRuleForm" 
            :rules="customerFormRules" 
            label-width="80px"
            ref="customerRuleFormRef">
                
                <el-form-item label="Firstname" prop="firstname">
                    <el-input v-model="customerRuleForm.firstname" clearable>
                    </el-input>
                </el-form-item>

                <el-form-item label="Lastname" prop="lastname" clearable>
                    <el-input v-model="customerRuleForm.lastname">
                    </el-input>
                </el-form-item>
                
            </el-form>

            <section style="display: flex; flex-direction: row-reverse; gap: 16px; margin-bottom: 1.2rem; width: 100;">
                <el-button @click="showCustomerForm = !showCustomerForm" :disabled="loadingForm" type="warning">Cancel</el-button>
                <el-button @click="createCustomer" type="primary" :disabled="loadingForm">Submit</el-button>
            </section>
        </el-dialog>
    </div>

    <script>
        const { createApp, ref } = Vue
        const ElNotification = ElementPlus.ElNotification
        const ElLoading = ElementPlus.ElLoading

        var phpData = <?php echo json_encode([
            'users' => $users, 
            'payrolls' => $payrolls, 
            'customers' => $customers, 
            'csrfToken' => csrf_hash()
        ]); ?>

        const app = createApp({
            setup() {
                const activeIndex = ref('3')
                const users = ref(phpData.users)
                const customers = ref(phpData.customers)
                const payrolls = ref(phpData.payrolls)
                const loadingForm = ref(false)
                const ruleFormRef = ref(null)
                const showForm = ref(false)
                const showCustomerForm = ref(false)
                const customerRuleFormRef = ref(null)
                const ruleForm = ref({
                    user_id: null,
                    commision_rate: null,
                    tax_rate: null,
                    sales: null,
                    bonus: null,
                    start: null,
                    end: null,
                    commision: null,
                    total: null,
                    customers: null,
                })
                const customerRuleForm = ref({
                    firstname: null,
                    lastname: null
                })
                const formRules = ref({
                    user_id: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                    commision_rate: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                    tax_rate: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                    customers: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                    sales: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                    start: [
                        { required: true, message: 'Required', trigger: 'blur' },
                    ],
                })

                const customerFormRules = ref({
                    firstname: [
                        { required: true, message: 'Required', trigger: 'blur' },
                        { min: 1, max: 70, message: 'Length should atleast be 1', trigger: 'change' },
                    ],
                    lastname: [
                        { required: true, message: 'Required', trigger: 'blur' },
                        { min: 1, max: 50, message: 'Length should atleast be 1', trigger: 'change' },
                    ],
                })

                function handleSelect(value) {
                    if(value == '1') {
                        window.location.href = 'index.php';
                    }
                    else if(value == '2') {
                        window.location.href = 'users';
                    }
                    else if(value == '3') {
                        window.location.href = 'payroll';
                    }
                    else if(value == '5') {
                        window.location.href = 'logout';
                    }
                }

                function onAdd() {
                    console.log('onAdd')
                    ruleForm.value = {
                        user_id: null,
                        commision_rate: null,
                        tax_rate: null,
                        sales: null,
                        bonus: null,
                        start: null,
                        end: null,
                        commision: null,
                        total: null,
                        customers: null,
                    }
                    showForm.value = true
                }

                function onUserSelected(value) {
                    console.log("onUserSelected", value)

                    if(value) {
                        let selectedCustomer = users.value && users.value.find(li => li.id == value)

                        ruleForm.value.commision_rate = selectedCustomer && selectedCustomer.commision_rate
                        ruleForm.value.tax_rate = selectedCustomer && selectedCustomer.tax_rate
                    }
                }

                function onCustomerSelected(value) {
                    if(value.includes(0)) {
                        ruleForm.value.customers = ruleForm.value.customers.filter(li => li != 0)
                        customerRuleForm.value = {
                            firstname: null,
                            lastname: null
                        }
                        showCustomerForm.value = true
                        updateTotals()
                    }
                }
                
                function updateTotals() {
                    let sales = (ruleForm.value.sales || 0)
                    let commission = sales * (ruleForm.value.commision_rate/100)
                    let tax = commission * (ruleForm.value.tax_rate/100)
                    let bonus = ruleForm.value.bonus || 0

                    commission = commission - tax
                    
                    let total = commission + bonus

                    ruleForm.value.commision = commission
                    ruleForm.value.total = total ? Number(total.toFixed(2)) : total
                }

                async function onSubmit() {
                    let valid = await ruleFormRef.value.validate()

                    updateTotals()

                    if(valid) {
                        let startDate = ruleForm.value.start
                        let endDate = new Date((new Date(startDate)).setDate(startDate.getDate() + 6))

                        loadingForm.value = true
                        const loadingInstance = ElLoading.service({ fullscreen: true })
                        axios.defaults.headers.common['X-CSRF-TOKEN'] = phpData.csrfToken

                        var formdata = new FormData()
                        formdata.append('user_id', ruleForm.value.user_id)
                        formdata.append('commision_rate', ruleForm.value.commision_rate)
                        formdata.append('tax_rate', ruleForm.value.tax_rate)
                        formdata.append('sales', ruleForm.value.sales)
                        formdata.append('bonus', ruleForm.value.bonus)
                        formdata.append('commision', ruleForm.value.commision)
                        formdata.append('total', ruleForm.value.total)
                        formdata.append('start', startDate)
                        formdata.append('end', endDate)

                        ruleForm.value.customers.forEach((li, index) => {
                            formdata.append('customers['+index+']', li)
                        })

                        axios.post(
                            '/payroll', 
                            formdata,
                            { 
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }
                        ).then(response => {
                            console.log("onSubmit 4", endDate)

                            if(response.data.success && response.data.data) {
                                let result = response.data.data
                                payrolls.value.push(result)
                                console.log("onSubmit 5", result)
                            }

                            showForm.value = false
                        })
                        .catch(error => {
                            console.error(error);
                            ElNotification({
                                title: 'Error',
                                message: 'Something went wrong.',
                                type: 'error',
                            })
                        }).finally(() => {
                            loadingForm.value = false
                            loadingInstance.close()
                        })
                    }
                }

                async function createCustomer() {
                    console.log("createCustomer")
                    let valid = await customerRuleFormRef.value.validate()

                    if(valid) {
                        console.log("createCustomer 2")
                        loadingForm.value = true
                        const loadingInstance = ElLoading.service({ fullscreen: true })
                        axios.defaults.headers.common['X-CSRF-TOKEN'] = phpData.csrfToken

                        var formdata = new FormData()
                        formdata.append('firstname', customerRuleForm.value.firstname)
                        formdata.append('lastname', customerRuleForm.value.lastname)

                        axios.post(
                            '/payroll/customer', 
                            formdata,
                            { 
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }
                        ).then(response => {
                            if(response.data.success && response.data.data) {
                                let result = response.data.data

                                customers.value.push(response.data.data)

                                if(ruleForm.value.customers && ruleForm.value.customers.length) {
                                    ruleForm.value.customers.push(response.data.data.id)
                                } else {
                                    ruleForm.value.customers = [response.data.data.id]
                                }
                            }

                            showCustomerForm.value = false
                        })
                        .catch(error => {
                            console.error(error);
                            ElNotification({
                                title: 'Error',
                                message: 'Something went wrong.',
                                type: 'error',
                            })
                        }).finally(() => {
                            loadingForm.value = false
                            loadingInstance.close()
                        })
                    }
                }
                
                return {
                    activeIndex,
                    handleSelect,
                    onCustomerSelected,
                    onUserSelected,
                    onSubmit,
                    showForm,
                    ruleForm,
                    formRules,
                    showCustomerForm,
                    ruleFormRef,
                    customerRuleForm,
                    customerRuleFormRef,
                    customerFormRules,
                    loadingForm,
                    onAdd,
                    users,
                    customers,
                    payrolls,
                    createCustomer,
                    updateTotals,
                }
            }
        })

        app.use(ElementPlus);
        
        app.mount('#app')
    </script>
</body>


<?php include('common/footer.php'); ?>