<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/users', 'User::index');
$routes->post('/users', 'User::store');
$routes->delete('/users/(:num)', 'User::destroy/$1');
$routes->get('/payroll', 'Payroll::index');
$routes->post('/payroll', 'Payroll::store');
$routes->post('/payroll/customer', 'Payroll::storeCustomer');

$routes->get('/logout', 'Home::logout');

service('auth')->routes($routes);
