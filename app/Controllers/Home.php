<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        $db = \Config\Database::connect();

        $salesQuery = $db->query("
            SELECT * FROM payrolls 
            LEFT JOIN users AS sales_rep
                ON sales_rep.id = payrolls.user_id
            WHERE sales = (SELECT MAX(sales) FROM payrolls)
        ");

        $commissionQuery = $db->query("
            SELECT * FROM payrolls 
            LEFT JOIN users AS sales_rep
                ON sales_rep.id = payrolls.user_id
            WHERE total = (SELECT MAX(total) FROM payrolls)
        ");        

        $highestSales = $salesQuery->getResult();
        $highestCommission = $commissionQuery->getResult();

        $data = [
            'sales' => $highestSales,
            'commission' => $highestCommission,
        ];

        return view('dashboard', $data);
    }

    public function logout() {
        auth()->logout();
        return redirect()->to('login');
    }
}
