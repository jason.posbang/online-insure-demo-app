<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Payroll extends BaseController
{

    public function index(): string
    {
        $userModel = new \App\Models\UserModel();
        //$payrollModel = new \App\Models\PayrollModel();
        $customerModel = new \App\Models\CustomerModel();

        $payrolls = $this->getPayrolls();
        $users = $userModel->where('deleted_at', null)->findAll();
        //$payrolls = $payrollModel->findAll();
        $customers = $customerModel->findAll();

        //return json_encode($payrolls);

        return view(
            'payroll', 
            [ 
                'users' => $users, 
                'payrolls' => $payrolls, 
                'customers' => $customers 
            ]
        );
    }

    public function getPayrolls($id = null) {
        $db = \Config\Database::connect();

        $whereClause = $id ? "WHERE payrolls.id = $id " : "";

        $query = $db->query("SELECT 
                payrolls.*, 
                users.firstname AS 'user_firstname', users.lastname AS 'user_lastname',
                customers.*, 
                payroll_customers.payroll_id AS 'payroll_id',  
                customers.id AS 'customer_id'
            FROM payrolls
            LEFT JOIN payroll_customers 
                ON payroll_customers.payroll_id = payrolls.id
            LEFT JOIN customers 
                ON customers.id = payroll_customers.customer_id 
            LEFT JOIN users
	            ON users.id = payrolls.user_id
            $whereClause
            ORDER BY payrolls.id DESC
        ");

        $queryResult = $query->getResult();

        $formattedData = [];

        //creates an associative array
        foreach ($queryResult as $row) {
            
            $payrollId = $row->payroll_id;
            $userId = $row->user_id;

            $customer = [
                'id' => $row->customer_id,
                'firstname' => $row->firstname,
                'lastname' => $row->lastname,
            ];
            
            if (!isset($formattedData[$payrollId])) {
                $formattedData[$payrollId] = [
                    'id' => $row->payroll_id,
                    'firstname' => $row->user_firstname,
                    'lastname' => $row->user_lastname,
                    'commision_rate' => $row->commision_rate,
                    'tax_rate' => $row->tax_rate,
                    'sales' => $row->sales,
                    'bonus' => $row->bonus,
                    'commision' => $row->commision,
                    'total' => $row->total,
                    'start' => $row->start,
                    'end' => $row->end,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }

            $formattedData[$payrollId]['customers'][] = $customer;
        }

        //convert associative array to array [item, item, item]
        return array_values($formattedData);
    }

    public function store()
    {   
        $payrollModel = new \App\Models\PayrollModel();
        $payrollCustomerModel = new \App\Models\PayrollCustomerModel();

        $startDate = $this->request->getPost('start');
        $dateParts = preg_split('/\s+/', $startDate);
        $newStartdate = \DateTime::createFromFormat(
            'D M d Y H:i:s',
            $dateParts[0] . ' ' . $dateParts[1] . ' ' . $dateParts[2] . ' ' . $dateParts[3] . ' ' . $dateParts[4]
        );
        $formattedStartDate = $newStartdate->format('Y-m-d H:i:s');

        $endDate = $this->request->getPost('end');
        $dateParts = preg_split('/\s+/', $endDate);
        $newEnddate = \DateTime::createFromFormat(
            'D M d Y H:i:s',
            $dateParts[0] . ' ' . $dateParts[1] . ' ' . $dateParts[2] . ' ' . $dateParts[3] . ' ' . $dateParts[4]
        );
        $formattedEndDate = $newEnddate->format('Y-m-d H:i:s');

        $data = [
            'user_id' => $this->request->getPost('user_id'),
            'commision_rate' => $this->request->getPost('commision_rate'),
            'tax_rate' => $this->request->getPost('tax_rate'),
            'sales' => $this->request->getPost('sales'),
            'bonus' => $this->request->getPost('bonus'),
            'commision' => $this->request->getPost('commision'),
            'total' => $this->request->getPost('total'),
            'start' => $formattedStartDate,
            'end' => $formattedEndDate,
        ];

        $customers = $this->request->getPost('customers');
        $success = $payrollModel->insert($data);
        $id = $payrollModel->getInsertID();

        if($success && $customers && count($customers)) {
            foreach($customers as $customerId) {
                $payrollCustomerModel->insert(
                    ['payroll_id' => $id, 'customer_id' => $customerId]
                );
            }

            $data = $this->getPayrolls($id);
        }

        // $payrollModel = new \App\Models\PayrollModel();
        // $data = $payrollModel->find($id);

        $data = $this->getPayrolls($id);
        
        return $this->response->setJSON([
            'success' => $success,
            'data' => $data ? ($data[0] ?? $data) : $data,
            'id' => $id,
        ]);
    }

    public function storeCustomer()
    {   
        $customerModel = new \App\Models\CustomerModel();
        $data = [
            'firstname' => $this->request->getPost('firstname'),
            'lastname' => $this->request->getPost('lastname'),
        ];

        $success = $customerModel->insert($data);
        $id = $customerModel->getInsertID();
        $customerModel = new \App\Models\CustomerModel();
        $data = $customerModel->find($id);

        return $this->response->setJSON([
            'success' => $success,
            'data' => $data,
        ]);
    }
}
