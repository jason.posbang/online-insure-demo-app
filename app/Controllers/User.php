<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class User extends BaseController
{

    public function index(): string
    {
        $userModel = new \App\Models\UserModel();

        $results = $userModel->where('deleted_at', null)->findAll();

        return view('users', [ 'users' => $results ]);
    }

    public function store()
    {   
        $id = $this->request->getPost('id');
        $edit = $id != null && $id != 'null' ? true : false;

        $userModel = new \App\Models\UserModel();

        $data = [
            'username' => $this->request->getPost('username'),
            'firstname' => $this->request->getPost('firstname'),
            'lastname' => $this->request->getPost('lastname'),
            'commision_rate' => $this->request->getPost('commision_rate'),
            'tax_rate' => $this->request->getPost('tax_rate'),
            'active' => 1
        ];

        if($edit) {
            $id = $this->request->getPost('id');
            $success = $userModel->update($id, $data);
            $userModel = new \App\Models\UserModel();
            $data = $userModel->find($id);
        } else {
            $success = $userModel->insert($data);
            $id = $userModel->getInsertID();
            $userModel = new \App\Models\UserModel();
            $data = $userModel->find($id);
        }

        return $this->response->setJSON([
            'success' => $success,
            'data' => $data,
            'edit' => $edit
        ]);
    }

    public function destroy($id) {
        $userModel = new \App\Models\UserModel();
        
        return $this->response->setJSON([
            'success' => $userModel->delete($id),
            'id' => $id
        ]);
    }
}
